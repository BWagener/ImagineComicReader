﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Comics;
using System.ComponentModel;

namespace UnitTestProject
{
    [TestClass]
    public class ViewModelUnitTests
    {
        readonly string BaseUrl = "http://192.168.1.82:32500";

        [TestMethod]
        public async Task UpdateLibraryTest()
        {
            Comics.ComicLibraryViewModel cl = new ComicLibraryViewModel();

            Console.WriteLine("Starting Test");

            await cl.UpdateLibrary(BaseUrl);

            Console.WriteLine("Ending Test Num " + cl.ComicsList.Count);

            Assert.Fail();
        }
    }
}
