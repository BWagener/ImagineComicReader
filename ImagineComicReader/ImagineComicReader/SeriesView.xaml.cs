﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImagineComicReader
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SeriesView : ContentPage
    {
        public SeriesView()
        {
            InitializeComponent();
            SeriesListView.ItemsSource = ((App)Application.Current).ComicLibrary.SeriesItemList;

            if (((App)Application.Current).ComicLibrary.SeriesItemList.Count > 0)
            {
                Title = ((App)Application.Current).ComicLibrary.SeriesItemList[0].series;
            }
        }

        void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
            => ((ListView)sender).SelectedItem = null;

        async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }

            await Navigation.PushModalAsync(new ImagineComicReader.PageView());

            await ((App)Application.Current).ComicLibrary.GetFile("http://192.168.1.82:32500", ((Comic)e.SelectedItem));

            ((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }
    }

}
