﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

using ComicStreamer;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Net.Http;

namespace ImagineComicReader
{
    public class Comic
    {
        public int id { get; set; }
        public string added_ts { get; set; }
        public string series { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public string deleted_ts { get; set; }
        public string title { get; set; }
        public int issue { get; set; }
        public string storyarc { get; set; }
        public int lastread_page { get; set; }
        public string publisher { get; set; }
        public int page_count { get; set; }

        public Byte[] thumbnail { get; set; }
        public string filename { get; set; }
        public Byte[] file { get; set; }

    }

    public class Page
    {
        public Byte[] content;
        public int page_num { get; set; }

        public Page()
        {
            page_num = -1;
        }

        public Page(Byte[] a, int b)
        {
            content = a;
            page_num = b;
        }
    }

    public class ComicLibraryViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Comic> ComicsList = new ObservableCollection<Comic>();
        public ObservableCollection<Comic> SeriesItemList = new ObservableCollection<Comic>();
        public ObservableCollection<Page> LoadedPages = new ObservableCollection<Page>();

        public ComicLibraryViewModel()
        {
            Comic defaultComic = new Comic();
            defaultComic.id = -1;
            defaultComic.series = "Loading...";

            ComicsList.Add(defaultComic);
        }

        public async Task UpdateLibrary(string asBaseUrl, string series  = "")
        {
            ComicStreamerSharp cs = new ComicStreamerSharp(asBaseUrl);

            System.Diagnostics.Debug.WriteLine("UpdateLibrary");

            if (series == "")
            {
                ComicsList.Clear();
            }
            else
            {
                SeriesItemList.Clear();
            }

            try
            {

                // Get Issue List
                JObject list = await cs.GetComicList(series,per_page: "100");
                JArray comics = (JArray)list["comics"];

                System.Diagnostics.Debug.WriteLine("Found " + comics.Count + " comics");

                for (int i = 0; i < comics.Count; i++)
                {

                    var test = comics[i].ToString(Formatting.Indented);

                    // TODO: Add to sql database.


                    Comic lsComic = new Comic();

                    lsComic.id = (int)comics[i]["id"];
                    lsComic.added_ts = (string)comics[i]["added_ts"];
                    lsComic.deleted_ts = (string)comics[i]["deleted_ts"];
                    lsComic.issue = (int)comics[i]["issue"];

                    if (comics[i]["lastread_page"] != null)
                    {
                        /// lsComic.lastread_page = comics[i].Value<int?>("lastread_page") ?? 0;
                    }

                    if (comics[i]["month"] != null)
                    {
                        //                    lsComic.month = (int)comics[i]["month"];
                    }

                    lsComic.publisher = (string)comics[i]["publisher"];
                    lsComic.series = (string)comics[i]["series"];
                    lsComic.title = (string)comics[i]["title"];
                    lsComic.year = (int)comics[i]["year"];

                    lsComic.page_count = (int)comics[i]["page_count"];

                    if (comics[i]["storyarcs"] != null)
                    {
                        JArray length = (JArray)comics[i]["storyarcs"];
                        var len = length.Count;
                        if (len > 0)
                        {
                            lsComic.storyarc = (string)comics[i]["storyarcs"][0];
                        }
                    }


                    lsComic.thumbnail = await cs.GetThumbnail(lsComic.id);

                    if (series == "")
                    {
                        ComicsList.Add(lsComic);
                    }
                    else
                    {
                        SeriesItemList.Add(lsComic);
                    }
                }
            }
            catch (HttpRequestException e)
            {
                System.Diagnostics.Debug.WriteLine("HTTP Exception caught during update.", e.InnerException.Message);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception caught during update.", 
                    e.InnerException.Message);
            }

            if (series == "")
            {
                OnPropertyChanged("ComicsList");
            }
            else
            {
                OnPropertyChanged("SeriesItemList");
            }

            return;
        }

        public async Task GetFile(string asBaseUrl, Comic comicFileWanted)
        {
            ComicStreamerSharp cs = new ComicStreamerSharp(asBaseUrl);

            System.Diagnostics.Debug.WriteLine("GetFile");

            LoadedPages.Clear();

            try
            {
                for (int i = 0; i < comicFileWanted.page_count; i++)
                {
                    Page page = new Page();
                    page.page_num = i;
                    page.content = await cs.GetComicPage(comicFileWanted.id, 0);
                    
                    LoadedPages.Add(page);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception caught during get file.",
                    e.InnerException.Message);
            }

            OnPropertyChanged("LoadedPages");
        }



        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        /*
        public class Grouping<K, T> : ObservableCollection<T>
        {
            public K Key { get; private set; }

            public Grouping(K key, IEnumerable<T> items)
            {
                Key = key;
                foreach (var item in items)
                    this.Items.Add(item);
            }
        }
        

    class SeriesViewViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Item> Items { get; }
        public ObservableCollection<Grouping<string, Item>> ItemsGrouped { get; }

        public SeriesViewViewModel()
        {
            Items = new ObservableCollection<Item>(new[]
            {
                new Item { Text = "Baboon", Detail = "Africa & Asia" },
                new Item { Text = "Capuchin Monkey", Detail = "Central & South America" },
                new Item { Text = "Blue Monkey", Detail = "Central & East Africa" },
                new Item { Text = "Squirrel Monkey", Detail = "Central & South America" },
                new Item { Text = "Golden Lion Tamarin", Detail= "Brazil" },
                new Item { Text = "Howler Monkey", Detail = "South America" },
                new Item { Text = "Japanese Macaque", Detail = "Japan" },
            });

            var sorted = from item in Items
                         orderby item.Text
                         group item by item.Text[0].ToString() into itemGroup
                         select new Grouping<string, Item>(itemGroup.Key, itemGroup);

            ItemsGrouped = new ObservableCollection<Grouping<string, Item>>(sorted);

            RefreshDataCommand = new Command(
                async () => await RefreshData());
        }

        public ICommand RefreshDataCommand { get; }

        async Task RefreshData()
        {
            IsBusy = true;
            //Load Data Here
            await Task.Delay(2000);

            IsBusy = false;
        }

        bool busy;
        public bool IsBusy
        {
            get { return busy; }
            set
            {
                busy = value;
                OnPropertyChanged();
                ((Command)RefreshDataCommand).ChangeCanExecute();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName]string propertyName = "") =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        
    }*/
    }
}
