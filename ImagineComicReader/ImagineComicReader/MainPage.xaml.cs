﻿using Xamarin.Forms;

namespace ImagineComicReader
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            LibraryView.ItemsSource = ((App)Application.Current).ComicLibrary.ComicsList;

        }

        async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }

            var series = ((Comic)e.SelectedItem).series;
            await ((App)Application.Current).ComicLibrary.UpdateLibrary("http://192.168.1.82:32500", series);

            await Navigation.PushAsync(new ImagineComicReader.SeriesView());

            //DisplayAlert("Item Selected", ((Comic)e.SelectedItem).id.ToString(), "Ok");
            ((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }

    }
}
