﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ImagineComicReader
{
    public partial class App : Application
    {
        public ComicLibraryViewModel ComicLibrary;

        public App()
        {
            InitializeComponent();

            ComicLibrary = new ComicLibraryViewModel();

            MainPage = new NavigationPage(new ImagineComicReader.MainPage());
        }

        protected override async void OnStart()
        {
            // Handle when your app starts
            // TODO: https://forums.xamarin.com/discussion/25881/using-async-in-xamarin-forms
            string test = "hello";
            System.Diagnostics.Debug.WriteLine(test);

            try
            {
                await ComicLibrary.UpdateLibrary("http://192.168.1.82:32500");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception caught during update.", e);
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
